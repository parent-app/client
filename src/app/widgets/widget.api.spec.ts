import { TestBed } from '@angular/core/testing';

import { WidgetApi } from './widget.api';

describe('WidgetApi', () => {
  let service: WidgetApi;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WidgetApi);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
