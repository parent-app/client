import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { FeedItem } from '../../model/feed-item';
import { WidgetApi } from '../widget.api';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-rss-feed-widget',
  templateUrl: './rss-feed-widget.component.html',
  styleUrls: ['./rss-feed-widget.component.scss'],
})
export class RssFeedWidgetComponent implements OnInit {

  isLoading = true;
  isError = false;
  rssFeedItems$: Observable<FeedItem[]>;

  constructor(private readonly widgetApi: WidgetApi) { }

  ngOnInit() {
    this.loadFeedItems();
  }

  ionViewWillEnter() {
    this.loadFeedItems();
  }

  loadFeedItems() {
    this.isError = false;
    this.rssFeedItems$ = this.widgetApi.getParentsNews().pipe(
      tap(_ => this.isLoading = false),
      catchError(_ => {
        this.isLoading = false;
        this.isError = true;
        return of([]);
      })
    );
  }
}
