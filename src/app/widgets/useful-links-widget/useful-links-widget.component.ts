import { Component, OnInit } from '@angular/core';

interface LinkItem {
  title: string;
  url: string;
}

@Component({
  selector: 'app-useful-links-widget',
  templateUrl: './useful-links-widget.component.html',
  styleUrls: ['./useful-links-widget.component.scss'],
})
export class UsefulLinksWidgetComponent implements OnInit {

  links: LinkItem[] = [
    {
      title: 'Grossesse : démarches et accompagnement (Ameli)',
      url: 'https://www.ameli.fr/assure/droits-demarches/famille/maternite-paternite-adoption/grossesse'
    },
    {
      title: 'Femme enceinte : prise en charge à 100% (Service public)',
      url: 'https://www.service-public.fr/particuliers/vosdroits/F164'
    },
    {
      title: 'La durée du congé maternité d\'une salariée (Ameli)',
      url: 'https://www.ameli.fr/assure/droits-demarches/famille/duree-du-conge-maternite/conge-maternite-salariee'
    },
    {
      title: 'La durée du congé maternité d\'une travailleuse indépendante (Ameli)',
      url: 'https://www.ameli.fr/assure/droits-demarches/famille/duree-du-conge-maternite/conge-maternite-independante'
    },
    {
      title: 'Le congé de paternité et d’accueil de l’enfant (Ameli)',
      url: 'https://www.ameli.fr/assure/droits-demarches/famille/maternite-paternite-adoption/conge-paternite-accueil-enfant'
    },
    {
      title: 'Congé parental d’éducation : conditions et protection sociale (Ameli)',
      url: 'https://www.ameli.fr/assure/droits-demarches/famille/maternite-paternite-adoption/conge-parental-education'
    },
    {
      title: 'Grossesse : examens médicaux ? (Service public)',
      url: 'https://www.service-public.fr/particuliers/vosdroits/F963'
    }
  ];

  constructor() { }

  ngOnInit() {}

}
