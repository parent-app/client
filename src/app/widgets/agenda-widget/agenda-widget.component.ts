import { Component, OnDestroy, OnInit } from '@angular/core';
import { TokenStorageService } from '../../shared/token-storage.service';
import { WidgetApi } from '../widget.api';
import { Observable, Subscription } from 'rxjs';
import { Appointment } from '../../model/appointment';
import { tap } from 'rxjs/operators';
import { DATE_PATTERN, TIME_PATTERN } from '../../constants';

@Component({
  selector: 'app-agenda-widget',
  templateUrl: './agenda-widget.component.html',
  styleUrls: ['./agenda-widget.component.scss'],
})
export class AgendaWidgetComponent implements OnInit, OnDestroy {

  userId: string;
  appointments$: Observable<Appointment[]>;
  subscription: Subscription;
  datePattern = DATE_PATTERN;
  timePattern = TIME_PATTERN;

  constructor(private readonly tokenStorageService: TokenStorageService,
              private readonly widgetApi: WidgetApi) { }

  ngOnInit() {
    this.subscription = this.tokenStorageService.getId().pipe(
      tap(userId => {
        if (userId) {
          this.userId = userId;
          this.loadAppointments();
        }
      })
    ).subscribe();
  }

  loadAppointments() {
    this.appointments$ = this.widgetApi.getUserNextAppointments(this.userId);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
