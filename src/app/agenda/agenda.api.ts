import { Injectable } from '@angular/core';
import { AGENDA_API_URL } from '../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Appointment } from '../model/appointment';
import { createRequestOption } from '../shared/requestUtils';

@Injectable({
  providedIn: 'root'
})
export class AgendaApi {
  resourceUrl = `${AGENDA_API_URL}/appointments`;

  constructor(protected readonly http: HttpClient) { }

  getUserAppointments(userId: string): Observable<Appointment[]> {
    const options = createRequestOption({userId});
    return this.http.get<Appointment[]>(this.resourceUrl, {params: options});
  }

  createAppointment(appointment: Appointment): Observable<Appointment> {
    return this.http.post<Appointment>(this.resourceUrl, appointment);
  }

  updateAppointment(appointment: Appointment): Observable<Appointment> {
    return this.http.put<Appointment>(`${this.resourceUrl}/${appointment.id}`, appointment);
  }

  deleteAppointment(appointmentId: number): Observable<HttpResponse<any>> {
    return this.http.delete(`${this.resourceUrl}/${appointmentId}`, {observe: 'response'});
  }
}
