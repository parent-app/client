import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CalendarComponent } from 'ionic2-calendar';
import { AlertController, ModalController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { CalendarModalPage } from './calendar-modal/calendar-modal.page';
import { CalendarEvent, calendarEventFromAppointment } from '../model/calendar-event';
import { Observable, of, Subscription } from 'rxjs';
import { AgendaApi } from './agenda.api';
import { map, tap } from 'rxjs/operators';
import { Appointment } from '../model/appointment';
import { TokenStorageService } from '../shared/token-storage.service';
import { getAppointmentTypeFromStringValue } from '../shared/enum.utils';

enum SaveMethod { CREATE, UPDTATE }

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.page.html',
  styleUrls: ['./agenda.page.scss'],
})
export class AgendaPage implements OnInit, OnDestroy {

  @ViewChild(CalendarComponent, {static: false}) myCalendar: CalendarComponent;

  calendar = {
    mode: 'month',
    currentDate: new Date(),
    locale: 'fr-FR'
  };

  currentMonth: string;
  eventSource$: Observable<CalendarEvent[]>;
  subscriptions: Subscription[];
  appointments: Appointment[];
  userId: string;

  constructor(private readonly alertCtrl: AlertController,
              private readonly modalCtrl: ModalController,
              private readonly agendaApi: AgendaApi,
              private readonly tokenStorageService: TokenStorageService) {
    this.subscriptions = [];
  }

  ngOnInit() {
   this.tokenStorageService.getId().pipe(
     map(userId => {
       if (userId) {
         this.userId = userId;
         this.agendaApi.getUserAppointments(userId).pipe(
           map((data: Appointment[]) => {
             this.appointments = data;
             this.setEventSource();
           } )
         ).subscribe();
       }
     })
   ).subscribe();
  }

  next() {
    this.myCalendar.slideNext();
  }

  back() {
    this.myCalendar.slidePrev();
  }

  async onEventSelected(event) {
    const start = formatDate(event.startTime, 'medium', this.calendar.locale);
    const end = formatDate(event.endTime, 'medium', this.calendar.locale);
    const alert = await this.alertCtrl.create({
      header: event.title,
      subHeader: event.desc,
      message: 'Du ' + start + '<br><br>Au ' + end,
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          cssClass: 'primary'
        },
        {
          text: 'Modifier',
          cssClass: 'warning',
          handler: _ => this.openCalendarModal(event)
        },
        {
          text: 'Supprimer',
          cssClass: 'danger',
          handler: _ => this.deleteAppointment(event.id)
        }
      ]
    });
    await alert.present();
  }

  onViewTitleChanged(title: string) {
    this.currentMonth = title;
  }

  async openCalendarModal(calendarEvent: CalendarEvent = null) {
    const saveMethod = calendarEvent !== null ? SaveMethod.UPDTATE : SaveMethod.CREATE;
    const modal = await this.modalCtrl.create({
      component: CalendarModalPage,
      cssClass: 'modal-css',
      backdropDismiss: false,
      componentProps: {
        title: calendarEvent !== null ? getAppointmentTypeFromStringValue(calendarEvent.title) : null,
        description: calendarEvent?.desc || null,
        startTime: calendarEvent?.startTime || null,
        endTime: calendarEvent?.endTime || null
      }
    });

    await modal.present();

    modal.onDidDismiss().then((result) => {
      if (result.data && result.data.event) {
        const event = {...calendarEvent, ...result.data.event};
        this.saveEvent(event, saveMethod);
      }
    });
  }

  private saveEvent(event: CalendarEvent, method: SaveMethod) {
    const appointment = new Appointment();
    appointment.AppointmentFromCalendarEvent(event, this.userId);
    if (method === SaveMethod.CREATE) {
      this.subscriptions.push(
        this.agendaApi.createAppointment(appointment).pipe(
          map(data => {
              this.appointments.push(data);
              this.setEventSource();
            }
          )
        ).subscribe()
      );
    } else {
      const index = this.appointments.findIndex(element => element.id === event.id);
      this.subscriptions.push(
        this.agendaApi.updateAppointment(appointment).pipe(
          map(data => {
            this.appointments.splice(index, 1, data);
            this.setEventSource();
          })
        ).subscribe()
      );
    }
  }

  private deleteAppointment(appointmentId: number) {
    const index = this.appointments.findIndex(element => element.id === appointmentId);
    this.subscriptions.push(
      this.agendaApi.deleteAppointment(appointmentId).pipe(
        tap((res: any) => {
          if (res.status === 204) {
            this.appointments.splice(index, 1);
            this.setEventSource();
          }
        })
      ).subscribe()
    );
  }

  private setEventSource(): void {
    const calendarEvents = [];
    this.appointments.forEach(element => calendarEvents.push(calendarEventFromAppointment(element)));
    this.eventSource$ = of(calendarEvents);
  }

  ngOnDestroy() { // TODO : voir si onDestroy ou à un autre évènement ionic
    // TODO : voir comment détruire les subsriptions de l'init (comment les fusionner en une)
    this.subscriptions.forEach(element => element.unsubscribe());
  }
}
