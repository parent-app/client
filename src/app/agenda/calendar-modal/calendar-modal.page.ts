import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AppointmentType } from '../../model/appointment-type.enum';

export interface CalEvent {
  title: string;
  desc: string;
  startTime: string;
  endTime: string;
}

@Component({
  selector: 'app-calendar-modal',
  templateUrl: './calendar-modal.page.html',
  styleUrls: ['./calendar-modal.page.scss'],
})
export class CalendarModalPage implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() startTime: string;
  @Input() endTime: string;

  event: CalEvent;
  appointmentType = AppointmentType;
  minEndDate: string;

  constructor(private modalCtrl: ModalController) {}

  ngOnInit() {
    this.event = {
      title: this.title,
      desc: this.description,
      startTime: this.startTime !== null ? new Date(this.startTime).toISOString() : null,
      endTime: this.endTime !== null ? new Date(this.endTime).toISOString() : null
    };
  }

  save() {
    this.modalCtrl.dismiss({event: this.event});
  }

  close() {
    this.modalCtrl.dismiss();
  }

  onStartTimeChange(date: string) {
    this.minEndDate = date;
  }
}
