import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AgendaPageRoutingModule } from './agenda-routing.module';
import { AgendaPage } from './agenda.page';
import { NgCalendarModule } from 'ionic2-calendar';
import { CalendarModalPage } from './calendar-modal/calendar-modal.page';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendaPageRoutingModule,
    NgCalendarModule,
    SharedModule
  ],
  declarations: [
    AgendaPage,
    CalendarModalPage
  ]
})
export class AgendaPageModule {}
