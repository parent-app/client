import { Injectable } from '@angular/core';
import { CHILD_GROWTH_API_URL } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GrowthStage } from '../../model/growth-stage';
import { createRequestOption } from '../../shared/requestUtils';
import { Period } from '../../model/period';
import { GrowthStageType } from '../../model/growth-stage-type';

export interface GrowthStageFilters {
  periods: number[];
  types: number[];
}

@Injectable({
  providedIn: 'root'
})
export class GrowthStageApi {
  rootUrl = `${CHILD_GROWTH_API_URL}/growth-stages`;
  periodUrl = `${this.rootUrl}/periods`;
  typeUrl = `${this.rootUrl}/types`;

  constructor(protected readonly http: HttpClient) {}

  getGrowthStages(req?: GrowthStageFilters): Observable<GrowthStage[]> {
    const options = createRequestOption(req);
    return this.http.get<GrowthStage[]>(this.rootUrl, {params: options});
  }

  getGrowthStagePeriods(): Observable<Period[]> {
    return this.http.get<Period[]>(this.periodUrl);
  }

  getGrowthStageTypes(): Observable<GrowthStageType[]> {
    return this.http.get<GrowthStageType[]>(this.typeUrl);
  }
}
