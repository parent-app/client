import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GrowthStagePage } from './growth-stage/growth-stage-page.component';
import { VaccinePage } from './vaccine/vaccine.page';
import { DietaryDiversificationPage } from './dietary-diversification/dietary-diversification.page';

const routes: Routes = [
  {
    path: 'growth-stage',
    component: GrowthStagePage
  },
  {
    path: 'vaccine',
    component: VaccinePage
  },
  {
    path: 'dietary-diversification',
    component: DietaryDiversificationPage
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChildGrowthRoutingModule {}
