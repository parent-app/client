import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DietaryDiversificationPage } from './dietary-diversification.page';

describe('DietaryDiversificationPage', () => {
  let component: DietaryDiversificationPage;
  let fixture: ComponentFixture<DietaryDiversificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietaryDiversificationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DietaryDiversificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
