import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Food } from '../../model/food';
import { AbstractEntity } from '../../model/abstract-entity';
import { DietaryDiversificationApi, FoodFilter } from './dietary-diversification.api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FoodSuggestion } from '../../model/food-suggestion';
import { map } from 'rxjs/operators';
import { IonicSelectableComponent } from 'ionic-selectable';

@Component({
  selector: 'app-dietary-diversification',
  templateUrl: './dietary-diversification.page.html',
  styleUrls: ['./dietary-diversification.page.scss'],
})
export class DietaryDiversificationPage implements OnInit {

  @ViewChild('ionicSelectableComponent') ionicSelectableComponent: IonicSelectableComponent;

  food$: Observable<Food[]>;
  period$: Observable<AbstractEntity[]>;
  type$: Observable<AbstractEntity[]>;
  foodFilterForm: FormGroup;
  selectedFood: Food;
  suggestedFood$: Observable<FoodSuggestion[]>;

  constructor(private readonly dietService: DietaryDiversificationApi,
              private readonly formBuilder: FormBuilder) { }

  ngOnInit() {
    this.period$ = this.dietService.getPeriods();
    this.type$ = this.dietService.getTypes();
    this.initForm();
  }

  initForm() {
    this.foodFilterForm = this.formBuilder.group({
      period: null,
      type: null
    });
  }

  onFilterChange() {
    this.ionicSelectableComponent.clear();
    const period = this.foodFilterForm.controls.period.value;
    const type = this.foodFilterForm.controls.type.value;
    if (period !== null || type !== null) {
      const filters: FoodFilter = {
        period,
        type
      };
      this.food$ = this.dietService.getFoods(filters);
    } else {
      this.food$ = of(null);
    }
  }

  onSearchFood(searchValue: string) {
    if (searchValue) {
      this.suggestedFood$ = this.dietService.suggestFood(searchValue);
    }
  }

  onSelectedFood() {
    if (this.selectedFood !== null) {
      this.food$ = this.dietService.getFood(this.selectedFood.id).pipe(
        map(data => [data])
      );
    }
  }

  onClearSuggestion() {
    this.ionicSelectableComponent.clear();
    this.food$ = null;
  }

  onClearFilter(formControlName: string) {
    this.foodFilterForm.controls[formControlName].setValue(null);
  }
}
