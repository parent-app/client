import { Injectable } from '@angular/core';
import { CHILD_GROWTH_API_URL } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Vaccine } from '../../model/vaccine';
import { createRequestOption } from '../../shared/requestUtils';
import { Period } from '../../model/period';

@Injectable({
  providedIn: 'root'
})
export class VaccineApi {
  rootUrl = `${CHILD_GROWTH_API_URL}/vaccines`;
  periodUlr = `${this.rootUrl}/periods`;

  constructor(protected readonly http: HttpClient) {}

  getVaccines(req?: number[]): Observable<Vaccine[]> {
    const options = createRequestOption(req);
    return this.http.get<Vaccine[]>(this.rootUrl, { params: options });
  }

  getPeriods(): Observable<Period[]> {
    return this.http.get<Period[]>(this.periodUlr);
  }
}
