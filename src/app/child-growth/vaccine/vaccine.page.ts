import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Vaccine } from '../../model/vaccine';
import { Period } from '../../model/period';
import { VaccineApi } from './vaccine.api';

@Component({
  selector: 'app-vaccine',
  templateUrl: './vaccine.page.html',
  styleUrls: ['./vaccine.page.scss'],
})
export class VaccinePage implements OnInit {

  vaccines$: Observable<Vaccine[]>;
  periods$: Observable<Period[]>;
  periodFilter: any = null;

  constructor(private readonly vaccineApi: VaccineApi) { }

  ngOnInit() {
    this.periods$ = this.vaccineApi.getPeriods();
    this.getVaccines();
  }

  getVaccines() {
    this.vaccines$ = this.vaccineApi.getVaccines(this.periodFilter);
  }

  onPeriodChange(event: CustomEvent) {
    this.periodFilter = {periods: event.detail.value};
    this.getVaccines();
  }

}
