import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './auth-guard.service';
import { AdminGuardService } from './admin-guard.service';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'home',
    canLoad: [AuthGuardService],
    canActivate: [AuthGuardService],
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'users',
    canLoad: [AuthGuardService],
    canActivate: [AuthGuardService],
    loadChildren: () => import('./user/user.module').then( m => m.UserPageModule)
  },
  {
    path: 'agenda',
    canLoad: [AuthGuardService],
    canActivate: [AuthGuardService],
    loadChildren: () => import('./agenda/agenda.module').then( m => m.AgendaPageModule)
  },
  {
    path: 'child-growth',
    canLoad: [AuthGuardService],
    canActivate: [AuthGuardService],
    loadChildren: () => import('./child-growth/child-growth.module').then( m => m.ChildGrowthPageModule)
  },
  {
    path: 'rss-feeds',
    canLoad: [AuthGuardService],
    canActivate: [AuthGuardService],
    loadChildren: () => import('./rss-feeds/rss-feeds.module').then( m => m.RssFeedsPageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
  ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
