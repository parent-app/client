import { Injectable } from '@angular/core';
import { USER_API_URL } from '../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Child } from '../model/child';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChildApi {
  resourceUrl = `${USER_API_URL}/children`;

  constructor(protected readonly http: HttpClient) { }

  saveChild(child: Child): Observable<Child> {
    if (child.id) {
      return this.updateChild(child);
    }
    return this.createChild(child);
  }

  createChild(child: Child): Observable<Child> {
    return this.http.post<Child>(this.resourceUrl, child);
  }

  updateChild(child: Child): Observable<Child> {
    return this.http.put<Child>(`${this.resourceUrl}/${child.id}`, child);
  }

  deleteChild(id: number): Observable<HttpResponse<any>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
