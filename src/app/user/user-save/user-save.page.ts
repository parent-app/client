import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserRole } from '../../model/user-role.enum';
import { User } from '../../model/user.model';
import { UserApi } from '../../shared/user.api';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { UserService } from '../../shared/user.service';
import { UserWithoutPassword } from '../../model/user-without-password.model';

@Component({
  selector: 'app-user-save',
  templateUrl: './user-save.page.html',
  styleUrls: ['./user-save.page.scss'],
})
export class UserSavePage implements OnInit, OnDestroy {

  userForm: FormGroup;
  userRoles = UserRole;
  subscriptions: Subscription[];
  user: UserWithoutPassword;
  saveMethod: string;

  constructor(private readonly formBuilder: FormBuilder,
              private readonly userApi: UserApi,
              private readonly router: Router,
              private readonly userService: UserService) {
    this.subscriptions = [];
  }

  ngOnInit() {
    this.subscriptions.push(
        this.userService.user$.pipe(
            map(element => this.user = element )
        ).subscribe()
    );
    this.subscriptions.push(
        this.userService.saveMethod$.pipe(
            map(element => this.saveMethod = element )
        ).subscribe()
    );
    this.initForm();
  }

  initForm() {
    this.userForm = this.formBuilder.group({
      username: [this.user ? this.user.username : '', Validators.required],
      email: [this.user ? this.user.email : '', Validators.required],
      password: ['', Validators.required],
      role: [this.user ? this.user.role : '', Validators.required]
    });
    if (this.saveMethod === 'update') {
      this.userForm.controls.password.disable();
    }
  }

  saveUser() {
    const formValues = this.userForm.value;
    const user = new User(formValues);

    if (this.saveMethod === 'update') {
      user.id = this.user.id;
      this.userApi.updateUser(user)
          .pipe(
              tap(() => this.router.navigate(['/users']))
          ).subscribe();
    } else {
      this.userApi.createUser(user)
          .pipe(
              tap(() => this.router.navigate(['/users']))
          ).subscribe();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach(element => element.unsubscribe());
  }

}
