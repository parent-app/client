import { NgModule } from '@angular/core';
import { UserPageRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserProfilPage } from './user-profil/user-profil.page';
import { UserListPage } from './user-list/user-list.page';
import { UserSavePage } from './user-save/user-save.page';
import { ChildSaveModalPage } from './child-save-modal/child-save-modal.page';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    UserPageRoutingModule,
    SharedModule,
    FormsModule
  ],
  declarations: [
    UserListPage,
    UserSavePage,
    UserProfilPage,
    ChildSaveModalPage
  ],
})
export class UserPageModule {}
