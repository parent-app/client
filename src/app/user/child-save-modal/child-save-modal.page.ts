import { Component, Input, OnInit } from '@angular/core';
import { Child } from '../../model/child';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-child-save',
  templateUrl: './child-save-modal.page.html',
  styleUrls: ['./child-save-modal.page.scss'],
})
export class ChildSaveModalPage implements OnInit {

  @Input() childInput: Child;
  child: Child;

  constructor(private readonly modalCtrl: ModalController) { }

  ngOnInit() {
    this.child = new Child(this.childInput);
    this.child.birthDate = this.childInput?.birthDate ? new Date(this.childInput.birthDate).toISOString() : null;
  }

  save() {
    this.modalCtrl.dismiss({child: this.child});
  }

  close() {
    this.modalCtrl.dismiss();
  }

}
