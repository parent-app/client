import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChildSaveModalPage } from './child-save-modal.page';

describe('ChildSaveModalPage', () => {
  let component: ChildSaveModalPage;
  let fixture: ComponentFixture<ChildSaveModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildSaveModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChildSaveModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
