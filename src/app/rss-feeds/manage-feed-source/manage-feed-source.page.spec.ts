import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ManageFeedSourcePage } from './manage-feed-source.page';

describe('ManageFeedSourcePage', () => {
  let component: ManageFeedSourcePage;
  let fixture: ComponentFixture<ManageFeedSourcePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageFeedSourcePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ManageFeedSourcePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
