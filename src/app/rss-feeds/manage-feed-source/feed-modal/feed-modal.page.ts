import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RssFeed } from '../../../model/rss-feed';

@Component({
  selector: 'app-feed-modal',
  templateUrl: './feed-modal.page.html',
  styleUrls: ['./feed-modal.page.scss'],
})
export class FeedModalPage implements OnInit {

  @Input() id: number;
  @Input() title: string;
  @Input() url: string;
  @Input() userId: string;

  rssFeed: RssFeed;

  constructor(private readonly modalCtrl: ModalController) { }

  ngOnInit() {
    this.rssFeed = new RssFeed(this.id, this.title, this.url, this.userId);
  }

  save() {
    this.modalCtrl.dismiss(this.rssFeed);
  }

  close() {
    this.modalCtrl.dismiss();
  }

}
