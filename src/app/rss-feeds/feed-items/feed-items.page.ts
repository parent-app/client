import { Component, OnDestroy, OnInit } from '@angular/core';
import { RssFeedsApi } from '../rss-feeds.api';
import { Observable, of, Subscription } from 'rxjs';
import { RssFeed } from '../../model/rss-feed';
import { FeedItem } from '../../model/feed-item';
import { TokenStorageService } from '../../shared/token-storage.service';
import { catchError, finalize, tap } from 'rxjs/operators';
import { SavedFeedItem } from '../../model/saved-feed-item';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-feed-items',
  templateUrl: './feed-items.page.html',
  styleUrls: ['./feed-items.page.scss'],
})
export class FeedItemsPage implements OnInit, OnDestroy {

  rssFeeds$: Observable<RssFeed[]>;
  feedItems$: Observable<FeedItem[]>;
  subscriptions: Subscription[] = [];
  userId: string;
  isLoading = false;
  toastMessage = '';
  toastColor = '';

  constructor(private readonly rssFeedsApi: RssFeedsApi,
              private readonly tokenStorageService: TokenStorageService,
              private readonly toastController: ToastController) { }

  ngOnInit() {
    this.subscriptions.push(this.tokenStorageService.getId().pipe(
      tap(userId => {
        if (userId) {
          this.userId = userId;
          this.loadRssFeeds();
        }
      })
    ).subscribe());
  }

  ionViewWillEnter() {
    this.loadRssFeeds();
  }

  ionViewDidLeave() {
    this.feedItems$ = of();
  }

  loadRssFeeds() {
    this.rssFeeds$ = this.rssFeedsApi.getFeedsSources(this.userId);
  }

  onFeedChange(feedId: number) {
    this.isLoading = true;
    this.feedItems$ = this.rssFeedsApi.readFeed(feedId, this.userId).pipe(
      tap(_ => this.isLoading = false),
      catchError(_ => {
        this.isLoading = false;
        this.toastMessage = 'Un problème est survenu lors de la récupération du flux. Veuillez réessayer plus tard.' +
          'S\'il s\'agit d\'un flux que vous avez ajouté, veuillez vérifier sa validité.';
        this.toastColor = 'danger';
        this.presentToast();
        return of([]);
      })
    );
  }

  onSaveFeedItem(feedItem: FeedItem) {
    const savedFeedItem = new SavedFeedItem(this.userId, feedItem.title, feedItem.url);
    this.subscriptions.push(this.rssFeedsApi.saveFeedItem(savedFeedItem).pipe(
      tap(_ => {
        this.toastMessage = 'Article sauvegardé';
        this.toastColor = 'success';
      }),
      catchError((err) => {
        this.toastColor = 'warning';
        this.toastMessage = err.error.message;
        return of();
      }),
      finalize(() => this.presentToast())
    ).subscribe());
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private async presentToast() {
    const toast = await this.toastController.create({
      message: this.toastMessage,
      duration: this.toastColor === 'success' ? 3000 : null,
      color: this.toastColor,
      buttons: [{
        side: 'end',
        icon: 'close-outline',
        role: 'cancel'
      }]
    });
    await toast.present();
  }

}
