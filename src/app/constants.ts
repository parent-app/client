// API URL
export const USER_API_URL = 'http://localhost:8081/api';
export const AGENDA_API_URL = 'http://localhost:8082/api';
export const CHILD_GROWTH_API_URL = 'http://localhost:8083/api';
export const RSS_FEEDS_API_URL = 'http://localhost:8084/api';

// DATE PATTERNS
export const DATE_TIME_PATTERN = 'dd/MM/yy HH:mm';
export const DATE_PATTERN = 'dd/MM/yy';
export const TIME_PATTERN = 'HH:mm';
