export interface PageRequest {
  page: number;
  size: number;
  direction?: SortDirection;
  unpaged?: boolean;
}

export type SortDirection = 'ASC' | 'DESC';
