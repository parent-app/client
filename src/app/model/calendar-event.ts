import { Appointment } from './appointment';
import { getAppointmentTypeStringValue } from '../shared/enum.utils';
import { AppointmentType } from './appointment-type.enum';

export interface CalendarEvent {
  title: string;
  desc: string;
  startTime: Date;
  endTime: Date;
  id?: number;
}

export function calendarEventFromAppointment(appointment: Appointment) {
  return {
    title: getAppointmentTypeStringValue(AppointmentType[appointment.type]),
    desc: appointment.description,
    startTime: new Date(appointment.startDate),
    endTime: new Date(appointment.endDate),
    id: appointment.id
  };
}
