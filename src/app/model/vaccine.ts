import { VaccinePeriod } from './vaccine-period';

export class Vaccine {
  id: number;
  name: string;
  description: string;
  mandatory: boolean;
  vaccinePeriods: VaccinePeriod[];
}
