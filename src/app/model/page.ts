export class Page<T> {
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  content: T[];
}
