export class Child {
  id: number;
  name: string;
  birthDate: string;
  userId: string;
  avatar: string;

  constructor(data: Partial<Child> = null) {
    Object.assign(this, data);
    this.avatar = data?.avatar || this.generateAvatar();
  }

  generateAvatar(): string {
    return 'assets/icon/avatar/' + Math.floor(Math.random() * (31 - 1) + 1) + '.png';
  }
}
