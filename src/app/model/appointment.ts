import { AppointmentType } from './appointment-type.enum';
import { CalendarEvent } from './calendar-event';
import { dateToIsoStringWithTimezone } from '../shared/date.utils';

export class Appointment {
  id: number;
  startDate: string;
  endDate: string;
  description: string;
  type: AppointmentType;
  userId?: string;

  constructor() {}

  AppointmentFromCalendarEvent(data: CalendarEvent, userId: string) {
    this.startDate = dateToIsoStringWithTimezone(data.startTime);
    this.endDate = dateToIsoStringWithTimezone(data.endTime);
    this.type = AppointmentType[data.title];
    this.description = data.desc;
    this.id = data.id;
    this.userId = userId;
  }
}
