import { Period } from './period';

export class VaccinePeriod {
  period: Period;
  boosterDose: boolean;
}
