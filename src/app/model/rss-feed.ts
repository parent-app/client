export class RssFeed {
  id: number;
  title: string;
  url: string;
  userId: string;

  constructor(id, title, url, userId) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.userId = userId;
  }
}
