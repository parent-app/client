import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AppointmentTypePipe } from './appointment-type.pipe';
import { FoodColorPipe } from './food-color.pipe';
import { ToolbarComponent } from './toolbar/toolbar.component';



@NgModule({
  declarations: [
    AppointmentTypePipe,
    FoodColorPipe,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    IonicModule,
    ReactiveFormsModule,
    AppointmentTypePipe,
    FoodColorPipe,
    ToolbarComponent
  ]
})
export class SharedModule { }
