export function dateToIsoStringWithTimezone(date: Date): string {
  const tzoffset = (new Date()).getTimezoneOffset() * 60000;
  return (new Date(new Date(date).getTime() - tzoffset)).toISOString().slice(0, -1);
}
