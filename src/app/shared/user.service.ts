import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserWithoutPassword } from '../model/user-without-password.model';
import { Child } from '../model/child';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject = new BehaviorSubject<UserWithoutPassword>(new UserWithoutPassword());
  private saveMethodSubject = new BehaviorSubject<string>('');
  private childSubject = new BehaviorSubject<Child>(new Child());

  user$ = this.userSubject.asObservable();
  saveMethod$ = this.saveMethodSubject.asObservable();
  child$ = this.childSubject.asObservable();

  constructor() {}

  setUser(user: UserWithoutPassword) {
    this.userSubject.next(user);
  }

  setSaveMethod(saveMethod: string) {
    this.saveMethodSubject.next(saveMethod);
  }

  setChild(child: Child) {
    this.childSubject.next(child);
  }
}
