import { Pipe, PipeTransform } from '@angular/core';
import { AppointmentType } from '../model/appointment-type.enum';
import { getAppointmentTypeStringValue } from './enum.utils';

@Pipe({name: 'appointmentType'})
export class AppointmentTypePipe implements PipeTransform {
  transform(value: string): string {
    return getAppointmentTypeStringValue(AppointmentType[value]);
  }
}
