import { Injectable } from '@angular/core';
import { USER_API_URL } from '../constants';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createRequestOption } from './requestUtils';
import { UserWithoutPassword } from '../model/user-without-password.model';
import { User } from '../model/user.model';
import { UserWithoutPasswordPage } from '../model/UserWithoutPasswordPage';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserApi {
    resourceUrl = `${USER_API_URL}/users`;

    constructor(protected readonly http: HttpClient) {}

    getUsers(req?: any): Observable<UserWithoutPasswordPage> {
        const options = createRequestOption(req);
        return this.http.get<any>(this.resourceUrl, {params : options});
    }

    getUserByExternalId(id: string): Observable<User> {
        return this.http.get<User>(`${this.resourceUrl}/${id}`).pipe(
          map(user => new User(user))
        );
    }

    createUser(user: User): Observable<UserWithoutPassword> {
        return this.http.post<UserWithoutPassword>(this.resourceUrl, user);
    }

    updateUser(user: User): Observable<UserWithoutPassword> {
        return this.http.put<UserWithoutPassword>(`${this.resourceUrl}/${user.id}`, user);
    }

    deleteUser(id: number): Observable<HttpResponse<any>> {
        return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'response'});
    }
}
