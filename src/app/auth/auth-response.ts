export interface AuthResponse {
    id: string;
    username: string;
    email: string;
    token: string;
    expiresIn: number;
    roles: string[];
}
