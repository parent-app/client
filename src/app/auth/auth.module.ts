import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../shared/shared.module';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';



@NgModule({
  declarations: [ LoginPage, RegisterPage ],
  imports: [
    CommonModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    AuthRoutingModule,
    SharedModule
  ]
})
export class AuthModule { }
