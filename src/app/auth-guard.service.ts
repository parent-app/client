import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { TokenStorageService } from './shared/token-storage.service';

@Injectable()
export class AuthGuardService implements CanLoad, CanActivate {

  constructor(private readonly tokenStorageService: TokenStorageService,
              private readonly router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.tokenStorageService.authenticadedSubject.value) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (!this.tokenStorageService.authenticadedSubject.value) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
