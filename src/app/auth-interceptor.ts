import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { TokenStorageService } from './shared/token-storage.service';
import { map, mergeAll } from 'rxjs/operators';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private readonly tokenStorageService: TokenStorageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.tokenStorageService.getToken().pipe(
      map(value => {
        if (value != null) {
          req = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + value)});
        }
        return next.handle(req);
      }),
      mergeAll()
    );
  }

}
